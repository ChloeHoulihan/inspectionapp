# InspectionApp
This is an inspection app for health workers to manage multiple inspections for multiple areas.

## Installation
You can clone the repo and open "InspectionKit.xcworkspace" to run InspectionApp.

## Usage
This app allows users to start, save incomplete and submit complete inspections for a selected area.

There is an initial login screen which restricts access to the app.

There are 3 main app areas represented by tabs on the tab bar.  History, Inspect and Account.

### Inspect
This tab allows the user to start, complete, save, resume and submit inspections.
The flow for this tab is as follows:
* Home: New/Load choice
* New flow: New -> Location choice -> Inspection choice -> Inspection Flow
* Load flow: Load -> Unsubmitted inspections choice -> Inspection Flow
* Inspection flow: Start -> Questions (1+) -> Review -> Home

Please note "Inspect" refers to the whole feature contained in the tab including create new, load and save. "Inspection" refers to just the inspection models and flow.

The "InspectionModule" and "InspectionSDK" could be kept in another repo, and pulled down using SPM, thus the naming "SDK". This would decouple the business logic from the front-end, and allow us to update the SDK version when back-end services change, without having to make changes to InspectionKit code (just bump SDK version).

### Account
This tab allows the user to log out, and view any account details.

### History
This tab allows the user to view submitted/historic inspections, downloaded from the system, organised by area and inspection type.

#### Schedule
This tab may be expanded to also show a schedule of planned inspections, or this feature may warrant its own tab.

## License
[MIT](https://choosealicense.com/licenses/mit/)


## Assumptions
* No 2 questions in a given Inspection have the same question text. This is a fair assumption given that for a single user they cannot interpret 2 identical questions differently, and as such the answer for both would be the same.
* I have obviously assumed the format of the JSON recieved from the API
* Minimum iOS version of devices is iOS 14.5
