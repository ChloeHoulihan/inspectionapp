//
//  HistoryCoordinator.swift
//  InspectionApp
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import SwiftUI
import UIComponents

/// A Router in charge of setting up and maintaining the History flow: Routers, views and VMs.
class HistoryRouter: Router {
    func instantiateRootView() -> some View {
        instantiateHistoryView()
    }
    
    func instantiateHistoryView() -> some View {
        // TODO: Replace placeholder with history flow
        //let viewModel = HistoryViewModel()
        //let view = HistoryView(router: self, viewModel: viewModel)
        return ContentView()
    }
    
    func instantiateTabLabel() -> some View {
        // TODO: Replace with history tab bar label
        return Label("History", systemImage: "calendar")
    }
}
