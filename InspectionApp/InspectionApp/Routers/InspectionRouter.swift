//
//  InspectionRouter.swift
//  InspectionApp
//
//  Created by Chloe Houlihan on 17/07/2021.
//

import AppComponents
import InspectModule
import InspectUI
import SwiftUI
import UIComponents

/// A Router in charge of setting up and maintaining the Inspect/Inspection flow: Views and VMs.
class InspectionRouter: Router {
    @Published var screen: InspectRouterScreen
    private var appContext: Context
    
    init(appContext: Context, screen: InspectRouterScreen) {
        self.appContext = appContext
        self.screen = screen
    }
    
    /// For protocol conformance: Do not use
    func instantiateRootView() -> some View {
        return EmptyView() // TODO: Error view
    }
    
    /// This Router is special because its root view requires a VM to be injected
    func instantiateRootView(viewModel: InspectionViewModel) -> some View {
        return InspectionView(router: self, viewModel: viewModel)
    }
}

/// Provides the functions requested by the InspectionView, QuestionView and ReviewView
extension InspectionRouter: InspectionViewRouter, QuestionViewRouter, ReviewViewRouter {
    func instantiateQuestionView(viewModel: InspectionViewModel) -> QuestionView {
        return QuestionView(router: self, viewModel: viewModel)
    }
    
    func instantiateReviewView(viewModel: InspectionViewModel) -> ReviewView {
        return ReviewView(router: self, viewModel: viewModel)
    }
    
    func instantiateSelectionView() {
        screen = .selection // Change Inspect root view
    }
}
