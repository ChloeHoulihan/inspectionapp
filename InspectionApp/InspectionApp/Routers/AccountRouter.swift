//
//  AccountCoordinator.swift
//  InspectionApp
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import AccountModule
import AccountUI
import AppComponents
import SwiftUI
import UIComponents

/// A Router in charge of setting up and maintaining the Account flow: Views and VMs.
class AccountRouter: Router {
    private var appContext: Context
    
    init(appContext: Context) {
        self.appContext = appContext
    }
    
    func instantiateRootView() -> some View {
        instantiateAccountView()
    }
    
    func instantiateAccountView() -> some View {
        let viewModel = AccountViewModel(context: appContext)
        return AccountView(viewModel: viewModel)
    }
    
    func instantiateTabLabel() -> AccountLabel {
        return AccountLabel()
    }
}
