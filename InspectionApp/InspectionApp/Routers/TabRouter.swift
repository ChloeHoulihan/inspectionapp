//
//  TabRouter.swift
//  InspectionApp
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import InspectModule
import SwiftUI
import UIComponents
import AppComponents

enum Tab: Int {
    case history = 0
    case inspect = 1
    case account = 2
}

/// A Router in charge of setting up and maintaining the Tab Bar items: Routers, views and VMs.
class TabRouter: Router {
    private var appContext: Context
    
    /// Router that manages the Account flow when the user switches to the Account tab
    lazy private var accountRouter: AccountRouter = {
        return AccountRouter(appContext: appContext)
    }()

    /// Router that manages the History flow when the user switches to the History tab
    lazy private var historyRouter: HistoryRouter = {
        return HistoryRouter()
    }()
    
    /// Router that manages the Inspect flow when the user switches to the Inspect tab
    lazy private var inspectRouter: InspectRouter = {
        return InspectRouter(appContext: appContext)
    }()
    
    init(appContext: Context) {
        self.appContext = appContext
    }
    
    func instantiateRootView() -> some View {
        return TabRouterScreen(router: self)
    }
    
    func instantiateHistoryView() -> some View {
        return historyRouter.instantiateRootView()
    }
    
    func historyLabel() -> some View {
        return historyRouter.instantiateTabLabel()
    }
    
    func instantiateInspectView() -> some View {
        return inspectRouter.instantiateRootView()
    }
    
    func inspectLabel() -> some View {
        return inspectRouter.instantiateTabLabel()
    }
    
    func instantiateAccountView() -> some View {
        return accountRouter.instantiateRootView()
    }
    
    func accountLabel() -> some View {
        return accountRouter.instantiateTabLabel()
    }
}

struct TabRouterScreen: View {
    @StateObject var router: TabRouter
    @State private var selection: Tab = .inspect
    
    var body: some View {
        TabView(selection: $selection) {
            router.instantiateHistoryView()
                .tabItem {
                    router.historyLabel()
                }
                .tag(Tab.history)

            router.instantiateInspectView()
                .tabItem {
                    router.inspectLabel()
                }
                .tag(Tab.inspect)
            
            router.instantiateAccountView()
                .tabItem {
                    router.accountLabel()
                }
                .tag(Tab.account)
        }
    }
}
