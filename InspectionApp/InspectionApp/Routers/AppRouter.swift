//
//  AppRouter.swift
//  InspectionApp
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import AccountModule
import AccountUI
import AppComponents
import Combine
import SwiftUI
import UIComponents

/// Screens that can be displayed by AppRouter
enum AppRouterScreen {
    case tabBar
    case login
}

/// "Root" Router controls all other Routers and app flow of InspectionApp
class AppRouter: Router {
    private var subscriptions = Set<AnyCancellable>()
    private var appContext: Context
    /// Allows this and subsequent Routers to change the root screen displayed by this Router
    @Published var screen: AppRouterScreen = .tabBar
    
    /// Router that manages the tab bar
    lazy private var tabRouter: TabRouter = {
        return TabRouter(appContext: appContext)
    }()
    
    init(appContext: Context) {
        self.appContext = appContext
        setSubscriptions()
    }
    
    /// Sets up any subscriptions to publishers
    private func setSubscriptions() {
        appContext.userIDPublisher.sink { [weak self] (user) in
            if user != nil {
                self?.screen = .tabBar
            } else {
                self?.screen = .login
            }
        }.store(in: &self.subscriptions)
    }
    
    func instantiateRootView() -> some View {
        return AppRouterView(router: self)
    }
    
    func instantiateLoginScreen() -> some View {
        let viewModel = LoginViewModel(context: appContext)
        return LoginView(viewModel: viewModel)
    }
    
    func instantiateTabBarView() -> some View {
        return tabRouter.instantiateRootView()
    }
}

/// A view to allow us to control which screen is shown as the root view of this Router
struct AppRouterView: View {
    @StateObject var router: AppRouter
    
    var body: some View {
        switch router.screen {
        case .login:
            router.instantiateLoginScreen()
        case .tabBar:
            router.instantiateTabBarView()
        }
    }
}
