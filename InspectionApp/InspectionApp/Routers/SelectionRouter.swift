//
//  SelectionRouter.swift
//  InspectionApp
//
//  Created by Chloe Houlihan on 17/07/2021.
//

import AppComponents
import Combine
import InspectionModule
import InspectModule
import InspectUI
import SwiftUI
import UIComponents

/// A Router in charge of setting up and maintaining the Inspect/Selection flow: Views and VMs.
class SelectionRouter: Router {
    @Published var screen: InspectRouterScreen
    private var appContext: Context
    
    init(appContext: Context, screen: InspectRouterScreen) {
        self.appContext = appContext
        self.screen = screen
    }
    
    func instantiateRootView() -> some View {
        let viewModel = SelectionViewModel()
        return SelectionView(router: self, viewModel: viewModel)
    }
}

/// Provides the functions requested by the SelectionView
extension SelectionRouter: SelectionViewRouter {
    func instantiateNewSelectionView() -> NewInspectionView {
        let dataProvider = InspectDataProvider(context: appContext)
        let viewModel = NewInspectionViewModel(dataProvider: dataProvider)
        return NewInspectionView(router: self, viewModel: viewModel)
    }
    
    func instantiateLoadInspectionView() -> LoadInspectionView {
        let viewModel = LoadInspectionViewModel(persistence: appContext.persistenceManager)
        return LoadInspectionView(router: self, viewModel: viewModel)
    }
}

/// Provides the functions requested by the NewInspectionView
extension SelectionRouter: NewInspectionViewRouter {
    func instantiateInspectionView(area: Area, inspection: Inspection) {
        let dataProvider = InspectDataProvider(context: appContext)
        guard let viewModel = InspectionViewModel(area: area, inspection: inspection, dataProvider: dataProvider, persistence: appContext.persistenceManager) else {
            return // TODO: Instantiate ErrorView
        }
        screen = .inspection(viewModel) // Change Inspect root view
    }
}

/// Provides the functions requested by the LoadInspectionView
extension SelectionRouter: LoadInspectionViewRouter {
    func instantiateInspectionView(memento: InspectionMemento) {
        let dataProvider = InspectDataProvider(context: appContext)
        guard let viewModel = InspectionViewModel(memento: memento, dataProvider: dataProvider, persistence: appContext.persistenceManager) else {
            return // TODO: Instantiate ErrorView
        }
        screen = .inspection(viewModel) // Change Inspect root view
    }
}

