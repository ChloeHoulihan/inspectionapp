//
//  InspectRouter.swift
//  InspectionApp
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import AppComponents
import Combine
import InspectModule
import InspectUI
import SwiftUI
import UIComponents

/// Screens that can be displayed by InspectRouter
enum InspectRouterScreen {
    case selection
    case inspection(InspectionViewModel)
}

/// A Router in charge of setting up and maintaining the Inspect flow: Routers, views and VMs.
class InspectRouter: Router {
    @Published var screen: InspectRouterScreen
    
    private var appContext: Context
    private var subscriptions = Set<AnyCancellable>()
    
    /// Router for the Inspection flow where the user can start, fill in, submit and save an inspection.
    lazy private var inspectionRouter: InspectionRouter = {
        return InspectionRouter(appContext: appContext, screen: screen)
    }()
    
    /// Router for the Selection flow where the user can load a saved inspection, or select a new one.
    lazy private var selectionRouter: SelectionRouter = {
        return SelectionRouter(appContext: appContext, screen: screen)
    }()

    init(appContext: Context) {
        self.appContext = appContext
        self.screen = .selection
        
        setSubscriptions()
    }
    
    /// Sets up any subscriptions to publishers
    private func setSubscriptions() {
        // Allow the subsequent flows to access the root screen:
        self.selectionRouter.$screen.sink { [weak self] (screen) in
            self?.screen = screen
        }.store(in: &self.subscriptions)
        
        self.inspectionRouter.$screen.sink { [weak self] (screen) in
            self?.screen = screen
        }.store(in: &self.subscriptions)
    }
    
    func instantiateRootView() -> some View {
        return InspectRouterView(router: self)
    }
    
    func instantiateTabLabel() -> InspectLabel {
        return InspectLabel()
    }
    
    func instantiateSelectionScreen() -> some View {
        return selectionRouter.instantiateRootView()
    }
    
    func instantiateInspectionScreen(viewModel: InspectionViewModel) -> some View {
        return inspectionRouter.instantiateRootView(viewModel: viewModel)
    }
}

/// A view to allow us to control which screen is shown as the root view of this Router
struct InspectRouterView: View {
    @StateObject var router: InspectRouter
    
    var body: some View {
        switch self.router.screen {
        case .selection:
            self.router.instantiateSelectionScreen()
        case .inspection(let inspectionVM):
            self.router.instantiateInspectionScreen(viewModel: inspectionVM)
        }
    }
}
