//
//  InspectionApp.swift
//  InspectionApp
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import SwiftUI
import AppComponents
import ServicesSDK

@main
struct InspectionApp: App {
    @StateObject var appRouter = AppRouter(appContext: AppContext(services: MockServices(), persistenceManager: AppPersistenceManager()))
    
    var body: some Scene {
        WindowGroup {
            appRouter.instantiateRootView()
        }
    }
}
