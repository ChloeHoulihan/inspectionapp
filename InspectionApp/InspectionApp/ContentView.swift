//
//  ContentView.swift
//  InspectionApp
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import SwiftUI

// TODO: Delete this.

/// A placeholder view for missing features
struct ContentView: View {
    var body: some View {
        Text("This is a placeholder view")
            .padding()
    }
}

/// A placeholder tab bar label for missing features
struct ContentLabel: View {
    var body: some View {
        Label("Placeholder", systemImage: "book")
    }
}
