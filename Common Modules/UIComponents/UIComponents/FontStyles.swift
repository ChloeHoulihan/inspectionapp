//
//  FontStyles.swift
//  UIComponents
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import SwiftUI

/**
 * Normally I would have a ThemeManager. This would contain all the allowed colours mapped to names.
 * I would inject this through all the UI classes so we can enforce conformance to a theme/brand.
 */

public struct Title: ViewModifier {
    public func body(content: Content) -> some View {
        content
            .font(.system(size: Sizes.Font.title, weight: .semibold, design: .default))
            .foregroundColor(.blue)
    }
}

public struct Subtitle: ViewModifier {
    public func body(content: Content) -> some View {
        content
            .font(.system(size: Sizes.Font.subtitle, weight: .regular, design: .default))
            .foregroundColor(.black)
    }
}

public struct Button: ViewModifier {
    public func body(content: Content) -> some View {
        content
            .font(.system(size: Sizes.Font.subtitle, weight: .regular, design: .default))
            .foregroundColor(.white)
    }
}

public struct Paragraph: ViewModifier {
    public func body(content: Content) -> some View {
        content
            .font(.system(size: Sizes.Font.paragraph, weight: .regular, design: .default))
            .foregroundColor(.gray)
    }
}

public extension View {
    func titleStyle() -> some View {
        self.modifier(Title())
    }

    func subtitleStyle() -> some View {
        self.modifier(Subtitle())
    }
    
    func buttonStyle() -> some View {
        self.modifier(Button())
    }

    func paragraphStyle() -> some View {
        self.modifier(Paragraph())
    }
}
