//
//  Router.swift
//  InspectionApp
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import SwiftUI

/// The root of a pattern by David Gary Wood called the Router Pattern, it's very similar to the MVVM+C pattern we use at IG. I have adapted this pattern in this solution.
/// This reconciles an issue with navigation in SwiftUI where MVVM could not be achieved without the views knowing too much about each other, and views creating VMs.
/// Now, instead, the Router will handle creating Views and VMs, and Views can trigger flows via the Router.
public protocol Router: ObservableObject {
    associatedtype ViewOutput: View
    
    func instantiateRootView() -> ViewOutput
}
