//
//  SystemImages.swift
//  UIComponents
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import SwiftUI

/// Contains all previously used system images to avoid repeated strings
public enum SystemImages { // Acts as a namespace so we can easily browse the list of images
    
    // TODO: Add injectable colour/size/padding etc
    public struct RightChevron: View {
        
        public init() { }
        
        public var body: some View {
            Image(systemName: "chevron.right")
        }
    }
    
    // TODO: Add injectable colour/size/padding etc
    public struct Checkmark: View {
        
        public init() { }
        
        public var body: some View {
            Image(systemName: "checkmark")
        }
    }
    
    // TODO: Add injectable colour/size/padding etc
    public struct Trash: View {
        
        public init() { }
        
        public var body: some View {
            Image(systemName: "trash")
        }
    }
    
    // TODO: Add injectable colour/size/padding etc
    public struct Save: View {
        
        public init() { }
        
        public var body: some View {
            Image(systemName: "square.and.arrow.down")
        }
    }
    
    public struct Inspect: View {
        let size: CGFloat
        
        public init(size: CGFloat = 50) {
            self.size = size
        }
        
        public var body: some View {
            Image(systemName: "doc.on.clipboard")
                .resizable()
                .scaledToFit()
                .frame(height: size)
        }
    }
    
    public struct Account: View {
        let size: CGFloat
        
        public init(size: CGFloat = 50) {
            self.size = size
        }
        
        public var body: some View {
            Image(systemName: "person")
                .resizable()
                .scaledToFit()
                .frame(height: size)
        }
    }
    
    public struct Logout: View {
        let size: CGFloat
        
        public init(size: CGFloat = 25) {
            self.size = size
        }
        
        public var body: some View {
            Image(systemName: "arrow.right.square")
                .resizable()
                .scaledToFit()
                .frame(height: size)
        }
    }
}
