//
//  TickView.swift
//  UIComponents
//
//  Created by Chloe Houlihan on 22/07/2021.
//

import SwiftUI

/**
 * This would have been really cool, however SwiftUI does not recognise a Binding of Answer? as a Binding of CustomStringConvertible?.
 * There is a work around for this but it doesn't work for optionals.
 * I could have used generics (the original plan), but then I can't set the text for the each item.
 * I've left this here to demonstrate how I would normally attempt to make common Views reusable.
 */

public struct TickList: View {
    @Binding private var selected: CustomStringConvertible?
    private let list: [CustomStringConvertible]
    
    public init(list: [CustomStringConvertible], selected: Binding<CustomStringConvertible?>) {
        self.list = list
        _selected = selected
    }
    
    public var body: some View {
        ForEach(list, id: \.description) { item in
            TickRow(item: item, selected: $selected)
                .onTapGesture {
                    selected = item
                }
        }
    }
}

public struct TickRow: View {
    @Binding private var selected: CustomStringConvertible?
    private let item: CustomStringConvertible
    
    public init(item: CustomStringConvertible, selected: Binding<CustomStringConvertible?>) {
        self.item = item
        _selected = selected
    }
    
    public var body: some View {
        HStack(spacing: .zero) {
            Text(item.description).subtitleStyle()
                .foregroundColor(.blue)
            Spacer()
            if selected?.description == item.description {
                SystemImages.Checkmark().foregroundColor(.blue)
            }
        }
        .padding(.horizontal, Sizes.horizontalPadding)
        .padding(.vertical, Sizes.verticalPadding)
    }
}
