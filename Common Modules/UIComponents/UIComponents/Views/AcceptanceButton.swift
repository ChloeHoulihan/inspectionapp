//
//  AcceptanceButton.swift
//  UIComponents
//
//  Created by Chloe Houlihan on 22/07/2021.
//

import SwiftUI

public struct AcceptenceButtonStyle: ButtonStyle {
    @Environment(\.isEnabled) private var isEnabled
    
    public init() { }

    public func makeBody(configuration: Configuration) -> some View {
        configuration
            .label
            .buttonStyle()
            .foregroundColor(configuration.isPressed ? .gray : .white)
            .padding(10)
            .background(isEnabled ? Color.green : Color.gray)
            .cornerRadius(5)
    }
}
