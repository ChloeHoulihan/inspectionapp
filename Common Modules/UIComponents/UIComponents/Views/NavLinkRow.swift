//
//  TrailingImageHStack.swift
//  UIComponents
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import SwiftUI

/// A "row" intended for use in a VStack that has a right pointing chevron to indicate navigation to a new view when clicked
public struct NavLinkRow: View {
    private let text: String
    
    public init(text: String) {
        self.text = text
    }
    
    public var body: some View {
        HStack(spacing: .zero) {
            Text(text).subtitleStyle()
            Spacer()
            SystemImages.RightChevron()
        }
        .padding(.horizontal, Sizes.horizontalPadding)
        .padding(.vertical, Sizes.verticalPadding)
        .background(Color.white)
    }
}
