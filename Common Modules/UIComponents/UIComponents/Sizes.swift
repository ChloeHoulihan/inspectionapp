//
//  Sizes.swift
//  UIComponents
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import SwiftUI

/// Contains all of the values for sizing and spacing UI components such that they are consistent through-out the app
/// Normally each view, or related set of views, would define one of these. The values would be injectable and defaulted so each value can be adjusted by the caller.
public enum Sizes {
    public static let largePadding: CGFloat = 24
    
    public static let horizontalPadding: CGFloat = 16
    public static let verticalPadding: CGFloat = 8
    
    public struct Font {
        public static let title: CGFloat = 36
        public static let subtitle: CGFloat = 24
        public static let paragraph: CGFloat = 20
    }
}
