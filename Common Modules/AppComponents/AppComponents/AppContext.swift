//
//  AppContext.swift
//  AppComponents
//
//  Created by Chloe Houlihan on 17/07/2021.
//

import Combine
import ServicesSDK

/// A Context holds global information used by the app
public protocol Context {
    var persistenceManager: PersistenceManager { get }
    var services: Services { get }
    // TODO: Replace with loginManager:
    var userID: String? { get set }
    var userIDPublisher: Published<String?>.Publisher { get } // The only way to define publishers in protocols
}

/// This is the AppContext InspectionApp will use.
/// If we had multiple apps, we may use different Contexts for each one.
public class AppContext: Context {
    /// The persistence manager allows the app to save and load information
    public let persistenceManager: PersistenceManager
    /// The services allow the app to request and send information over the internet
    public let services: Services
    
    // TODO: Replace with a loginManager to allow us to verify users credentials
    @Published public var userID: String?
    public var userIDPublisher: Published<String?>.Publisher { $userID }
    
    public init(services: Services, persistenceManager: PersistenceManager) {
        self.persistenceManager = persistenceManager
        self.services = services
        self.userID = nil // Set during login
    }
}
