//
//  PersistenceManager.swift
//  AppComponents
//
//  Created by Chloe Houlihan on 21/07/2021.
//

import InspectionModule

// Allows us to save and retrieve data
public protocol PersistenceManager {
    func save(_ value: Any, for key: PersistenceKey)
    func load<Value>(with key: PersistenceKey) -> Value?
    func loadArray<Value>(with key: PersistenceKey) -> [Value]
}

public enum PersistenceKey: String {
    case inspections
}

public class AppPersistenceManager: PersistenceManager {
    private let defaults: UserDefaults
    
    public init() {
        self.defaults = UserDefaults.standard
    }
    
    // TODO: Save for user only: append userID to key
    public func save(_ value: Any, for key: PersistenceKey) {
        defaults.set(value, forKey: key.rawValue)
    }
    
    public func load<Value>(with key: PersistenceKey) -> Value? {
        guard let value = defaults.value(forKey: key.rawValue) as? Value else { return nil }
        return value
    }
    
    public func loadArray<Value>(with key: PersistenceKey) -> [Value] {
        guard let array: [Value] = load(with: key) else { return [] }
        return array
    }
}

// MARK:- Inspections

/// Handles saving and loading of Inspections
extension PersistenceManager {
    
    /// Load an array of encoded InspectionMementos
    private func loadRawInspections() -> [Data] {
        return loadArray(with: .inspections)
    }
    
    /// Loads and decodes any inspections stored
    public func loadInspections() -> [InspectionMemento] {
        let decoder = JSONDecoder()
        let data = loadRawInspections()
        var inspections: [InspectionMemento] = []
        
        for encodedInspection in data {
            do {
                let inspection = try decoder.decode(InspectionMemento.self, from: encodedInspection)
                inspections.append(inspection)
            } catch {
                continue
            }
        }
        
        return inspections
    }
    
    /// Encodes and appends inspection to the saved inspections
    /// - Parameter inspection: Inspection to add to saved inspections
    public func save(inspection: InspectionMemento) -> Bool {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(inspection)
            var savedInspections = loadRawInspections()
            // TODO: Check for uniqueness (only one submission for any area:inspection combo)
            savedInspections.append(data)
            save(savedInspections, for: .inspections)
        } catch {
            return false
        }
        
        return true
    }
}
