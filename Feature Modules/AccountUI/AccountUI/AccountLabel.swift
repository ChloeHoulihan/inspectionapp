//
//  AccountTabLabel.swift
//  AccountUI
//
//  Created by Chloe Houlihan on 22/07/2021.
//

import SwiftUI

public struct AccountLabel: View {
    
    public init() {}
    
    public var body: some View {
        Label("Account", systemImage: "person")
    }
}
