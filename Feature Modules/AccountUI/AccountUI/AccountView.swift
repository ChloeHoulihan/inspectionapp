//
//  AccountView.swift
//  AccountUI
//
//  Created by Chloe Houlihan on 22/07/2021.
//

import AccountModule
import SwiftUI
import UIComponents

public struct AccountView: View {
    @ObservedObject private var viewModel: AccountViewModel
    
    public init(viewModel: AccountViewModel) {
        self.viewModel = viewModel
    }
    
    public var body: some View {
        VStack {
            Text(viewModel.title).titleStyle()
                .padding(.horizontal, Sizes.horizontalPadding)
                .padding(.vertical, Sizes.verticalPadding)
            SystemImages.Account()
                .foregroundColor(.blue)
                .padding(.horizontal, Sizes.horizontalPadding)
                .padding(.vertical, Sizes.verticalPadding)
            Divider()
            Text(viewModel.username).subtitleStyle()
                .padding(.horizontal, Sizes.horizontalPadding)
                .padding(.vertical, Sizes.verticalPadding)
            
            // TODO: Populate with real data:
            VStack {
                Text("Occupation: Nurse")
                Divider()
                Text("Other information")
                Divider()
                Text("Other information")
                Divider()
                Text("Other information")
            }
            .padding(.horizontal, Sizes.horizontalPadding)
            .padding(.vertical, Sizes.verticalPadding)
            
            Divider()
            HStack {
                Text(viewModel.logoutText).subtitleStyle()
                Spacer()
                SystemImages.Logout()
            }
            .background(Color.white)
            .padding(.horizontal, Sizes.horizontalPadding)
            .padding(.vertical, Sizes.verticalPadding)
            .onTapGesture {
                viewModel.logout()
            }
            Divider()
            
            Spacer()
        }
    }
}
