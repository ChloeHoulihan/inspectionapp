//
//  LoginView.swift
//  AccountUI
//
//  Created by Chloe Houlihan on 22/07/2021.
//

import AccountModule
import SwiftUI
import UIComponents

public struct LoginView: View {
    @ObservedObject private var viewModel: LoginViewModel
    
    @State private var password: String = ""
    @State private var username: String = ""
    
    public init(viewModel: LoginViewModel) {
        self.viewModel = viewModel
    }
    
    public var body: some View {
        VStack(spacing: Sizes.verticalPadding) {
            Spacer()
            
            Text(viewModel.title).titleStyle()
            SystemImages.Inspect(size: 100)
                .foregroundColor(.blue)
                .padding(.all, Sizes.largePadding)
            
            HStack {
                Spacer()
                TextField(viewModel.usernameText, text: $username)
                    .padding(.vertical, Sizes.largePadding/2)
                    .frame(width: 300)
                Spacer()
            }
            .border(Color.gray)
            .padding(.horizontal, Sizes.largePadding)
            
            HStack {
                Spacer()
                SecureField(viewModel.passwordText, text: $password)
                    .padding(.vertical, Sizes.largePadding/2)
                    .frame(width: 300)
                Spacer()
            }
            .border(Color.gray)
            .padding(.horizontal, Sizes.largePadding)
            
            Text(viewModel.error == nil ? "" : viewModel.loginFailedText)
                .subtitleStyle()
            
            Button(viewModel.buttonText) {
                viewModel.login(userId: username, password: password)
            }
            .buttonStyle(AcceptenceButtonStyle())
            .disabled(username.isEmpty || password.isEmpty)
            
            Spacer()
        }
    }
}
