//
//  Questions.swift
//  InspectionModule
//
//  Created by Chloe Houlihan on 19/07/2021.
//

// TODO: Similar to Answers, create a common class using generics for them to inherit/conform to

/// Façade for an array of Question data that enforces conditions:
/// - Remove duplicates
/// - Remove nil data
/// - At least 1 question
public struct Questions {
    private let questions: [Question]
    
    public var count: Int {
        questions.count
    }
    
    public init?(_ questions: [Question?]) {
        guard !questions.isEmpty else { return nil }
        
        // Remove nil answers:
        var questions = questions.compactMap { $0 }
        
        // Remove duplicates:
        var buffer = Set<Question>()
        questions = questions.filter { buffer.insert($0).inserted }
        
        self.questions = questions
    }
    
    public func contains(_ question: Question) -> Bool {
        return  questions.contains(question)
    }

    public func getQuestion(at index: Int) -> Question? {
        guard index < questions.count else { return nil }
        return questions[index]
    }

    public func getIndex(for question: Question) -> Int? {
        return questions.firstIndex(of: question)
    }
    
    // Personal preference here. Questions.get() seems nicer than Questions.questions
    public func get() -> [Question] {
        return questions
    }
}

/// Allows us to store Questions
extension Questions: Codable { }
