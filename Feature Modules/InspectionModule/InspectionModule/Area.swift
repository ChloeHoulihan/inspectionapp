//
//  Area.swift
//  InspectionModule
//
//  Created by Chloe Houlihan on 19/07/2021.
//

/// Area data type has a unique area name and many applicable inspections
public struct Area {
    public let name: String
    public let inspections: Set<Inspection>
    
    public init?(name: String, inspections: Set<Inspection> = []) {
        guard !name.isEmpty else { return nil }
        self.name = name
        self.inspections = inspections
    }
}

extension Area: Hashable, Equatable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(name)
    }
    
    public static func == (lhs: Area, rhs: Area) -> Bool {
        return lhs.name == rhs.name
    }
}

/// Allows us to store an Area
extension Area: Codable { }
