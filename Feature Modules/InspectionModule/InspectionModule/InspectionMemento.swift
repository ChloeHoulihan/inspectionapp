//
//  InspectionMemento.swift
//  InspectionModule
//
//  Created by Chloe Houlihan on 20/07/2021.
//

/// Contains all the information we need to save and restore inspections
public struct InspectionMemento {
    public let area: Area
    public let currentIndex: Int
    public let inspection: Inspection
    public let result: InspectionResult
    
    public init(area: Area, currentIndex: Int, inspection: Inspection, result: InspectionResult) {
        self.area = area
        self.currentIndex = currentIndex
        self.inspection = inspection
        self.result = result
    }
}

extension InspectionMemento: Hashable, Equatable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(inspection.name)
        hasher.combine(area.name)
    }
    
    public static func == (lhs: InspectionMemento, rhs: InspectionMemento) -> Bool {
        return lhs.area.name == rhs.area.name &&
            lhs.inspection.name == rhs.inspection.name
    }
}

/// Allows us to store the memento
extension InspectionMemento: Codable { }
