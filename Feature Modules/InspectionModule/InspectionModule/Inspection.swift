//
//  Inspection.swift
//  InspectionModule
//
//  Created by Chloe Houlihan on 19/07/2021.
//

/// Inspection data type has a unique name and many questions (1+)
public struct Inspection {
    public let name: String
    public let questions: Questions
    
    public init?(name: String, questions: Questions) {
        guard !name.isEmpty else { return nil }
        self.name = name
        self.questions = questions
    }
}

extension Inspection: Hashable, Equatable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(name)
    }
    
    public static func == (lhs: Inspection, rhs: Inspection) -> Bool {
        return lhs.name == rhs.name
    }
}

/// Allows us to store Inspection
extension Inspection: Codable { }
