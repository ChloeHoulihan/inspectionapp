//
//  InspectionResult.swift
//  InspectionModule
//
//  Created by Chloe Houlihan on 19/07/2021.
//

public struct InspectionResult {
    // MARK:- Public fields
    
    /// Total score of inspection thus far
    public var currentScore: Int {
        questionAnswers.values.compactMap( { $0?.score } ).reduce(0, +)
    }
    /// Whether all questions are answered
    public var isComplete: Bool {
        return !questionAnswers.values.contains(nil)
    }
    /// Total possible score of the inspection
    public var maxScore: Int {
        applicableAnswerCount * 100
    }
    
    // MARK:- Private fields
    
    /// Questions mapped to answer, if answered, else nil
    private var questionAnswers: [Question: Answer?]
    /// Questions that are not N/A
    private var applicableQuestionAnswers: [Question: Answer?] {
        questionAnswers.filter { $0.value != Answer.notApplicable }
    }
    /// Number of applicable answers
    private var applicableAnswerCount: Int {
        applicableQuestionAnswers.count
    }
    
    // MARK:- Inits

    public init(questions: Questions) {
        self.questionAnswers = [:]
        let noAnswer: Answer? = nil
        for question in questions.get() {
            questionAnswers[question] = noAnswer
        }
    }
    
    // MARK:- Setters
    
    public mutating func setAnswer(_ answer: Answer, for question: Question) -> Bool {
        let isValid = questionAnswers.keys.contains(question) && question.answers.contains(answer)
        if isValid { questionAnswers[question] = answer }
        return isValid
    }
    
    // MARK:- Getters

    public func getAnswer(for question: Question) -> Answer? {
        return questionAnswers[question] ?? nil
    }
}

/// Allows us to store InspectionResult
extension InspectionResult: Codable { }
