//
//  InspectionModel.swift
//  InspectionModule
//
//  Created by Chloe Houlihan on 19/07/2021.
//

/// Contains all the information required to fill in an inspection and track progress
/// Intended for use by ViewModels
public struct InspectionModel {
    // MARK:- Public fields
    
    public let area: Area
    public let inspection: Inspection
    
    public var currentQuestion: Question? {
        questions.getQuestion(at: currentIndex)
    }
    public var isComplete: Bool {
        result.isComplete
    }
    public var isLastQuestion: Bool {
        currentIndex == inspection.questions.count - 1
    }
    public var maxScore: Int {
        result.maxScore
    }
    public var questions: Questions {
        inspection.questions
    }
    public var score: Int {
        result.currentScore
    }
    
    // MARK:- Private fields
    
    private var currentIndex: Int
    private var result: InspectionResult
    
    // MARK:- Inits
    
    public init?(area: Area, inspection: Inspection, result: InspectionResult? = nil, currentIndex: Int = 0) {
        guard area.inspections.contains(inspection) else { return nil } // TODO: Inspection not authorised
        
        self.area = area
        self.inspection = inspection
        // TODO: Ensure result questions match inspection questions
        self.result = result ?? InspectionResult(questions: inspection.questions)
        self.currentIndex = currentIndex
    }
    
    public init?(from memento: InspectionMemento) {
        self.init(area: memento.area, inspection: memento.inspection, result: memento.result, currentIndex: memento.currentIndex)
    }
    
    // MARK:- Setters
    
    // TODO: Change return Bools to Results
    
    public mutating func nextQuestion() -> Bool {
        let isValid = currentIndex < questions.count - 1
        currentIndex = isValid ? currentIndex + 1 : currentIndex
        return isValid
    }
    
    public mutating func previousQuestion() -> Bool {
        let isValid = currentIndex > 0
        currentIndex = isValid ? currentIndex - 1 : currentIndex
        return isValid
    }
    
    public mutating func answer(question: Question, with answer: Answer) -> Bool {
        return result.setAnswer(answer, for: question)
    }
    
    // MARK:- Getters
    
    public func getAnswer(for question: Question) -> Answer? {
        return result.getAnswer(for: question)
    }
    
    // MARK:- Export
    
    public func createMemento() -> InspectionMemento {
        return InspectionMemento(area: area, currentIndex: currentIndex, inspection: inspection, result: result)
    }
}
