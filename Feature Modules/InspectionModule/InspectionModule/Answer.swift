//
//  Answer.swift
//  InspectionModule
//
//  Created by Chloe Houlihan on 19/07/2021.
//

/// Answer data type handles the answer text and score, checking for validity
public struct Answer {
    public let answer: String
    public let score: Int
    
    public static let notApplicable = Answer(name: "Not applicable", validScore: 0)
    
    /// - Parameter answer: The text of the question to ask
    /// - Parameter score: The score between 0 and 100 the answer denotes
    public init?(_ answer: String, score: Int) {
        // Answer can't be empty:
        guard !answer.isEmpty else { return nil }
        // Score should be between 0 and 100
        guard (0...100).contains(score) else { return nil }
        
        self.answer = answer
        self.score = score
    }
    
    /// Allows us to make non-optional Answers from this class for values we already know are valid
    private init(name: String, validScore: Int) {
        self.answer = name
        self.score = validScore
    }
    
    public static func getScore(answers: [Answer?]) -> Int {
        return answers.map { $0?.score ?? 0 } .reduce(0, +)
    }
}

/// Only the text is considered for uniqueness to avoid having two answers that appear identical to the user, but have different scores
extension Answer: Hashable, Equatable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(answer)
    }
    
    public static func == (lhs: Answer, rhs: Answer) -> Bool {
        return lhs.answer == rhs.answer
    }
}

/// Allows us to store the answer
extension Answer: Codable { }
