//
//  Answers.swift
//  InspectionModule
//
//  Created by Chloe Houlihan on 19/07/2021.
//

/// A façade for an array of Answers that checks for conditions and applies certain requirements:
/// - At least 2 answers
/// - No nil answers
/// - No duplicate answers (for a single question)
/// - Adds the special "N/A" answer
public struct Answers {
    private let answers: [Answer]
    
    public var count: Int {
        answers.count
    }
    
    public init?(_ answers: [Answer?] = []) {
        // Must have at least 2 answers:
        guard answers.count > 1 else { return nil }
        
        // Remove nil answers:
        var answers = answers.compactMap { $0 }
        
        // Remove duplicates:
        var buffer = Set<Answer>()
        answers = answers.filter { buffer.insert($0).inserted }
        
        // Add special answer:
        answers.append(Answer.notApplicable)
        
        self.answers = answers
    }
    
    public func contains(_ answer: Answer) -> Bool {
        return answers.contains(answer)
    }

    public func getAnswer(at index: Int) -> Answer? {
        guard index < answers.count else { return nil }
        return answers[index]
    }

    public func getIndex(for answer: Answer) -> Int? {
        return answers.firstIndex(of: answer)
    }
    
    // Personal preference here. Answers.get() seems nicer than Answers.answers
    public func get() -> [Answer] {
        return answers
    }
}

/// Allows us to store the answers
extension Answers: Codable { }
