//
//  Question.swift
//  InspectionModule
//
//  Created by Chloe Houlihan on 19/07/2021.
//

/// Question data type holds the question text and possible answers
/// Acts as a façade for this tuple, automatically checking for required conditions
public struct Question {
    public let question: String
    public let answers: Answers
    
    public init?(_ question: String, answers: Answers) {
        // Question can't be empty:
        guard !question.isEmpty else { return nil }
        
        self.question = question
        self.answers = answers
    }
}

extension Question: Hashable, Equatable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(question)
    }
    
    public static func == (lhs: Question, rhs: Question) -> Bool {
        return lhs.question == rhs.question
    }
}

/// Allows us to store Question
extension Question: Codable { }
