//
//  QuestionTests.swift
//  InspectionModuleTests
//
//  Created by Chloe Houlihan on 19/07/2021.
//

import XCTest
import InspectionModule

class QuestionTests: XCTestCase {
    let answer1 = Answer("Answer", score: 5)
    let answer2 = Answer("Another answer", score: 50)
    let questionText = "This is a question"
    
    var answers: Answers!
    var question: Question!
    
    override func setUpWithError() throws {
        // Given:
        answers = Answers([answer1, answer2])
        
        // When:
        question = try XCTUnwrap(Question(questionText, answers: answers!))
    }
    
    func testInit() {
        // Then:
        XCTAssertEqual(question.question, questionText)
        XCTAssertEqual(question.answers.count, answers.count)
    }
    
    func testInitFailEmpty() {
        // When:
        let question = Question("", answers: answers!)
        
        // Then:
        XCTAssertNil(question)
    }
    
    func testUniqueness() {
        let answer3 = Answer("Another different answer", score: 100)
        
        // Given:
        let answers = Answers([answer1, answer2])
        let answersDifferent = Answers([answer1, answer2, answer3])
        
        // When:
        let question = Question("A question", answers: answers!)
        let questionDuplicate = Question("A question", answers: answers!)
        let questionDuplicateAnswers = Question("A different question", answers: answers!)
        let questionDuplicateQuestion = Question("A question", answers: answersDifferent!)
        let questionUnique = Question("A different question", answers: answersDifferent!)
        
        // Then:
        XCTAssertEqual(question, questionDuplicate)
        XCTAssertNotEqual(question, questionDuplicateAnswers)
        XCTAssertEqual(question, questionDuplicateQuestion)
        XCTAssertNotEqual(question, questionUnique)
    }
}
