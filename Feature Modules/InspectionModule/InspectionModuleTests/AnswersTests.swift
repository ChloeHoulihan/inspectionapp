//
//  AnswersTests.swift
//  InspectionModuleTests
//
//  Created by Chloe Houlihan on 19/07/2021.
//

import XCTest
import InspectionModule

class AnswersTests: XCTestCase {
    var answers: Answers!
    
    let answer = Answer("Test answer", score: 90)
    let answer2 = Answer("Test answer", score: 100)
    let answer3 = Answer("Another answer", score: 100)
    
    override func setUpWithError() throws {
        // Given:
        answers = Answers([answer!, answer2!, answer3!])
        // When:
        answers = try XCTUnwrap(answers)
    }
    
    func testInit() throws {
        // Then the expected answers are added:
        XCTAssertTrue(answers.contains(answer!))
        XCTAssertTrue(answers.contains(answer3!))
        
        // Then order is maintained:
        XCTAssertEqual(answers.getAnswer(at: 0)!.answer, answer!.answer)
        XCTAssertEqual(answers.getAnswer(at: 0)!.score, answer!.score)
        XCTAssertEqual(answers.getAnswer(at: 1)!.answer, answer3!.answer)
        XCTAssertEqual(answers.getAnswer(at: 1)!.score, answer3!.score)
        
        // Then N/A is added as last answer:
        XCTAssertTrue(answers.get().contains(where: { answer in answer.answer == Answer.notApplicable.answer }))
        XCTAssertEqual(answers.getAnswer(at: 2)!.answer, Answer.notApplicable.answer)
        XCTAssertEqual(answers.getAnswer(at: 2)!.score, Answer.notApplicable.score)
        
        // Then no additional answers are added:
        XCTAssertEqual(answers.get().count, 3, "Should have 3 answers: answer==answer2, answer3, N/A")
    }
    
    func testInitFailEmpty() throws {
        //Given:
        let empty: [Answer] = []
        
        // When:
        let answers = Answers(empty)
        
        // Then:
        XCTAssertNil(answers)
    }
    
    func testInitFailLow() throws {
        //Given:
        let low: [Answer] = [answer!]
        
        // When:
        let answers = Answers(low)
        
        // Then:
        XCTAssertNil(answers)
    }
    
    func testGet() {
        // When:
        let innerAnswers = answers.get()
        
        // Then:
        XCTAssertEqual(innerAnswers.count, 3)
        XCTAssertEqual(innerAnswers, [answer!, answer3!, Answer.notApplicable])
    }
    
    func testContains() {
        // When:
        let containsAnswer = answers.contains(answer!)
        let containsAnswer2 = answers.contains(answer2!)
        let containsBadAnswer = answers.contains(Answer("bad", score: 100)!)
        
        // Then:
        XCTAssertTrue(containsAnswer)
        XCTAssertTrue(containsAnswer2, "Should contain answer, which is equivalent to answer2")
        XCTAssertFalse(containsBadAnswer)
    }
    
    func testGetAnswer() {
        // When:
        let actualAnswer = answers.getAnswer(at: 0)
        let actualAnswer2 = answers.getAnswer(at: 2)
        let actualBadAnswer = answers.getAnswer(at: 10)
        
        // Then:
        XCTAssertEqual(actualAnswer, answer)
        XCTAssertEqual(actualAnswer2, Answer.notApplicable)
        XCTAssertNil(actualBadAnswer, "Index should not exist")
    }
    
    func testGetIndex() {
        // When:
        let actualAnswer = answers.getIndex(for: answer!)
        let actualAnswer2 = answers.getIndex(for: Answer.notApplicable)
        let actualBadAnswer = answers.getIndex(for: Answer("bad", score: 100)!)
        
        // Then:
        XCTAssertEqual(actualAnswer, 0)
        XCTAssertEqual(actualAnswer2, 2)
        XCTAssertNil(actualBadAnswer, "Answer should not exist")
    }
    
    func testCount() {
        // When:
        let count = answers.count
        
        // Then:
        XCTAssertEqual(count, 3)
    }
}
