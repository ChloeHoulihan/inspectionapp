//
//  InspectionModelTests.swift
//  InspectionModuleTests
//
//  Created by Chloe Houlihan on 20/07/2021.
//

import XCTest
import InspectionModule

class InspectionModelTests: XCTestCase {
    let answer1 = Answer("Answer 1", score: 50)!
    let answer2 = Answer("Answer 2", score: 100)!
    let answer3 = Answer("Answer 3", score: 5)!
    
    var question1: Question!
    var question2: Question!
    var question3: Question!
    var questions: Questions!
    
    var area: Area!
    var inspection: Inspection!
    
    var model: InspectionModel!
    
    override func setUp() {
        question1 = Question("Question 1", answers: Answers([answer1, answer2])!)
        question2 = Question("Question 2", answers: Answers([answer1, answer2])!)
        question3 = Question("Question 3", answers: Answers([answer1, answer3])!)
        
        questions = Questions([question1, question2, question3])
        inspection = Inspection(name: "Form A", questions: questions!)
        area = Area(name: "Ward A", inspections: [inspection!])
        
        model = InspectionModel(area: area, inspection: inspection)
    }

    func testInit() throws {
        let model = try XCTUnwrap(InspectionModel(area: area, inspection: inspection))
        
        // Then:
        XCTAssertEqual(model.area, area)
        XCTAssertEqual(model.inspection, inspection)
        XCTAssertEqual(model.currentQuestion, question1)
        
        XCTAssertFalse(model.isComplete)
        XCTAssertEqual(model.score, 0)
        XCTAssertEqual(model.maxScore, 300)
        
        XCTAssertTrue(model.questions.contains(question1))
        XCTAssertTrue(model.questions.contains(question2))
        XCTAssertTrue(model.questions.contains(question3))
    }
    
    func testInitFail() {
        // Given:
        let badInspection = Inspection(name: "Form B", questions: questions!)
        
        // When:
        let model = InspectionModel(area: area, inspection: badInspection!)
        
        // Then:
        XCTAssertNil(model)
    }
    
    func testInitWithResult() throws {
        // Given:
        var result = InspectionResult(questions: questions)
        _ = result.setAnswer(answer1, for: question1)
        
        // When:
        let model = try XCTUnwrap(InspectionModel(area: area, inspection: inspection, result: result))
        
        // Then:
        XCTAssertEqual(model.score, answer1.score)
    }
    
    func testInitWithIndex() throws {
        // Given:
        let currentIndex = 1
        
        // When:
        let model = try XCTUnwrap(InspectionModel(area: area, inspection: inspection, currentIndex: currentIndex))
        
        // Then:
        XCTAssertEqual(model.currentQuestion, question2)
    }
    
    func testInitFromMemento() throws {
        // Given:
        var result = InspectionResult(questions: questions)
        _ = result.setAnswer(answer1, for: question1)
        let memento = InspectionMemento(area: area, currentIndex: 1, inspection: inspection, result: result)
        
        // When:
        let model = try XCTUnwrap(InspectionModel(from: memento))
        
        // Then:
        XCTAssertEqual(model.area, area)
        XCTAssertEqual(model.inspection, inspection)
        XCTAssertEqual(model.currentQuestion, question2)
        
        XCTAssertFalse(model.isComplete)
        XCTAssertEqual(model.score, 50)
        XCTAssertEqual(model.maxScore, 300)
        
        XCTAssertTrue(model.questions.contains(question1))
        XCTAssertTrue(model.questions.contains(question2))
        XCTAssertTrue(model.questions.contains(question3))
    }
    
    func testInitFromMementoFail() {
        // Given:
        let badInspection = Inspection(name: "Form B", questions: questions!)
        let result = InspectionResult(questions: questions)
        let memento = InspectionMemento(area: area, currentIndex: 1, inspection: badInspection!, result: result)
        
        // When:
        let model = InspectionModel(from: memento)
        
        // Then:
        XCTAssertNil(model)
    }
    
    func testNextQuestion() {
        // When:
        var success = model.nextQuestion()
        
        // Then:
        XCTAssertTrue(success)
        XCTAssertEqual(model.currentQuestion, question2)
        
        // When:
        success = model.nextQuestion()
        
        // Then:
        XCTAssertTrue(success)
        XCTAssertEqual(model.currentQuestion, question3)
        
        // When:
        success = model.nextQuestion()
        
        // Then:
        XCTAssertFalse(success)
        XCTAssertEqual(model.currentQuestion, question3)
    }
    
    func testPreviousQuestion() {
        // When:
        var success = model.previousQuestion()
        
        // Then:
        XCTAssertFalse(success)
        XCTAssertEqual(model.currentQuestion, question1)
        
        // When:
        _ = model.nextQuestion()
        success = model.previousQuestion()
        
        // Then:
        XCTAssertTrue(success)
        XCTAssertEqual(model.currentQuestion, question1)
    }
    
    func testAnswer() {
        // When:
        let success = model.answer(question: question1, with: answer1)
        
        // Then:
        XCTAssertTrue(success)
        XCTAssertEqual(model.getAnswer(for: question1), answer1)
    }
    
    func testSetAnswerBadAnswer() {
        // Given:
        let badAnswer = Answer("bad", score: 100)!
        
        // When:
        let success = model.answer(question: question1, with: badAnswer)
        
        // Then:
        XCTAssertFalse(success)
        XCTAssertNil(model.getAnswer(for: question1))
    }
    
    func testSetAnswerBadQuestion() {
        // Given:
        let badQuestion = Question("bad", answers: Answers([answer1, answer2])!)!
        
        // When:
        let success = model.answer(question: badQuestion, with: answer1)
        
        // Then:
        XCTAssertFalse(success)
        XCTAssertNil(model.getAnswer(for: question1))
    }
    
    func testGetAnswer() {
        // When:
        let _ = model.answer(question: question1, with: answer1)
        let answer = model.getAnswer(for: question1)
        
        // Then:
        XCTAssertNotNil(answer)
        XCTAssertEqual(answer, answer1)
    }
    
    func testGetAnswerFail() {
        // Given:
        let badQuestion = Question("bad", answers: Answers([answer1, answer2])!)
        
        // When:
        let answer = model.getAnswer(for: badQuestion!)
        
        // Then:
        XCTAssertNil(answer)
    }
    
    func testCreateMemento() {
        // When:
        _ = model.nextQuestion()
        _ = model.answer(question: question1, with: answer1)
        let memento = model.createMemento()
        
        // Then:
        XCTAssertEqual(memento.area, area)
        XCTAssertEqual(memento.inspection, inspection)
        XCTAssertEqual(memento.currentIndex, 1)
        XCTAssertEqual(memento.result.currentScore, 50)
    }
}
