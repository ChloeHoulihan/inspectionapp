//
//  InspectionResultTests.swift
//  InspectionModuleTests
//
//  Created by Chloe Houlihan on 20/07/2021.
//

import XCTest
import InspectionModule

class InspectionResultTests: XCTestCase {
    let answer1 = Answer("Answer 1", score: 50)!
    let answer2 = Answer("Answer 2", score: 100)!
    let answer3 = Answer("Answer 3", score: 5)!
    
    var question1: Question!
    var question2: Question!
    var question3: Question!
    
    var questions: Questions!
    
    override func setUp() {
        question1 = Question("Question 1", answers: Answers([answer1, answer2])!)
        question2 = Question("Question 2", answers: Answers([answer1, answer2])!)
        question3 = Question("Question 3", answers: Answers([answer1, answer3])!)
        
        questions = Questions([question1, question2, question3])
    }

    func testInit() {
        // When:
        let result = InspectionResult(questions: questions)
        
        // Then public fields are set to initial values:
        XCTAssertFalse(result.isComplete)
        XCTAssertEqual(result.currentScore, 0)
        XCTAssertEqual(result.maxScore, 300)
        
        // Then answers are nil:
        XCTAssertEqual(result.getAnswer(for: question1), nil)
        XCTAssertEqual(result.getAnswer(for: question2), nil)
        XCTAssertEqual(result.getAnswer(for: question3), nil)
    }
    
    func testSetAnswer() {
        // Given:
        var result = InspectionResult(questions: questions)
        
        // When:
        let success = result.setAnswer(answer1, for: question1)
        
        // Then:
        XCTAssertTrue(success)
        XCTAssertEqual(result.getAnswer(for: question1), answer1)
    }
    
    func testSetAnswerBadAnswer() {
        // Given:
        var result = InspectionResult(questions: questions)
        let badAnswer = Answer("bad", score: 100)!
        
        // When:
        let success = result.setAnswer(badAnswer, for: question1)
        
        // Then:
        XCTAssertFalse(success)
        XCTAssertNil(result.getAnswer(for: question1))
    }
    
    func testSetAnswerBadQuestion() {
        // Given:
        var result = InspectionResult(questions: questions)
        let badQuestion = Question("bad", answers: Answers([answer1, answer2])!)!
        
        // When:
        let success = result.setAnswer(answer1, for: badQuestion)
        
        // Then:
        XCTAssertFalse(success)
        XCTAssertNil(result.getAnswer(for: question1))
    }
    
    func testIsComplete() {
        // Given:
        var result = InspectionResult(questions: questions)
        
        // When:
        _ = result.setAnswer(answer1, for: question1)
        _ = result.setAnswer(answer1, for: question2)
        _ = result.setAnswer(answer1, for: question3)
        
        // Then:
        XCTAssertTrue(result.isComplete, "No answers should be nil")
    }
    
    func testCurrentScore() {
        // Given:
        var result = InspectionResult(questions: questions)
        
        // When:
        _ = result.setAnswer(answer1, for: question1)
        _ = result.setAnswer(answer2, for: question2)
        _ = result.setAnswer(answer3, for: question3)
        
        // Then:
        XCTAssertEqual(result.currentScore, 155)
    }
    
    func testMaxScore() {
        // Given:
        var result = InspectionResult(questions: questions)
        
        // When:
        _ = result.setAnswer(Answer.notApplicable, for: question1)
        
        // Then:
        XCTAssertEqual(result.maxScore, 200)
        
        // When:
        _ = result.setAnswer(Answer.notApplicable, for: question2)
        
        // Then:
        XCTAssertEqual(result.maxScore, 100)
        
        // When:
        _ = result.setAnswer(Answer.notApplicable, for: question3)
        
        // Then:
        XCTAssertEqual(result.maxScore, 0)
    }
    
    func testGetAnswer() {
        // Given:
        var result = InspectionResult(questions: questions)
        
        // When:
        let _ = result.setAnswer(answer1, for: question1)
        let answer = result.getAnswer(for: question1)
        
        // Then:
        XCTAssertNotNil(answer)
        XCTAssertEqual(answer, answer1)
    }
    
    func testGetAnswerFail() {
        // Given:
        let result = InspectionResult(questions: questions)
        let badQuestion = Question("bad", answers: Answers([answer1, answer2])!)
        
        // When:
        let answer = result.getAnswer(for: badQuestion!)
        
        // Then:
        XCTAssertNil(answer)
    }
}
