//
//  AnswerTests.swift
//  InspectionModuleTests
//
//  Created by Chloe Houlihan on 19/07/2021.
//

import XCTest
@testable import InspectionModule

class AnswerTests: XCTestCase {
    func testInit() throws {
        // Given:
        let expectedScore = 45
        let expectedName = "Test answer"
        
        // When:
        let answer = Answer(expectedName, score: expectedScore)
        let unwrappedAnswer = try XCTUnwrap(answer)
        
        // Then:
        XCTAssertEqual(unwrappedAnswer.answer, expectedName)
        XCTAssertEqual(unwrappedAnswer.score, expectedScore)
    }
    
    func testInitLow() throws {
        // Given:
        let expectedScore = 0
        let expectedName = "Test answer"
        
        // When:
        let answer = Answer(expectedName, score: expectedScore)
        let unwrappedAnswer = try XCTUnwrap(answer)
        
        // Then:
        XCTAssertEqual(unwrappedAnswer.answer, expectedName)
        XCTAssertEqual(unwrappedAnswer.score, expectedScore)
    }
    
    func testInitHigh() throws {
        // Given:
        let expectedScore = 100
        let expectedName = "Test answer"
        
        // When:
        let answer = Answer(expectedName, score: expectedScore)
        let unwrappedAnswer = try XCTUnwrap(answer)
        
        // Then:
        XCTAssertEqual(unwrappedAnswer.answer, expectedName)
        XCTAssertEqual(unwrappedAnswer.score, expectedScore)
    }
    
    func testInitFailHigh() {
        // Given:
        let expectedScore = 101
        let expectedName = "Test answer"
        
        // When:
        let answer = Answer(expectedName, score: expectedScore)
        
        // Then:
        XCTAssertNil(answer)
    }
    
    func testInitFailLow() {
        // Given:
        let expectedScore = -1
        let expectedName = "Test answer"
        
        // When:
        let answer = Answer(expectedName, score: expectedScore)
        
        // Then:
        XCTAssertNil(answer)
    }
    
    func testInitFailEmpty() {
        // Given:
        let expectedScore = 5
        let expectedName = ""
        
        // When:
        let answer = Answer(expectedName, score: expectedScore)
        
        // Then:
        XCTAssertNil(answer)
    }
    
    func testGetScore() {
        // Given:
        let testName = "Test answer"
        let answer1 = Answer(testName, score: 0)
        let answer2 = Answer(testName, score: 5)
        let answer3 = Answer(testName, score: 100)
        
        let answers = [answer1, answer2, answer3]
        
        // When:
        let score = Answer.getScore(answers: answers)
        
        // Then:
        XCTAssertEqual(score, 105)
    }
    
    func testNotApplicable() {
        // Given:
        let answer = Answer.notApplicable
        
        // Then:
        XCTAssertNotNil(answer)
        XCTAssertEqual(answer.answer, "Not applicable")
        XCTAssertEqual(answer.score, 0)
    }
    
    func testUniqueness() {
        // Given:
        let answer = Answer("Answer", score: 5)
        let answerDuplicate = Answer("Answer", score: 5)
        let answerDuplicateScore = Answer("Another Answer", score: 5)
        let answerDuplicateName = Answer("Answer", score: 10)
        let answerUnique = Answer("Another Name", score: 5)
        
        // Then:
        XCTAssertEqual(answer, answerDuplicate)
        XCTAssertNotEqual(answer, answerDuplicateScore)
        XCTAssertEqual(answer, answerDuplicateName)
        XCTAssertNotEqual(answer, answerUnique)
    }
}
