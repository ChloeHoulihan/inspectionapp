//
//  QuestionsTests.swift
//  InspectionModuleTests
//
//  Created by Chloe Houlihan on 19/07/2021.
//

import XCTest
import InspectionModule

class QuestionsTests: XCTestCase {
    // OK to force unwrap because if these fail it suggests a problem in the Question class, not Questions
    // therefore the QuestionTests should fail
    var question1: Question!
    var question2: Question!
    var question3: Question!
    
    var answers: Answers!
    
    override func setUpWithError() throws {
        // Given:
        let answer1 = Answer("Answer", score: 5)
        let answer2 = Answer("Another answer", score: 50)
        answers = Answers([answer1, answer2])
        
        let questionText1 = "This is a question"
        let questionText2 = "This is a question"
        let questionText3 = "This is another question"
        question1 = Question(questionText1, answers: answers!)
        question2 = Question(questionText2, answers: answers!)
        question3 = Question(questionText3, answers: answers!)
    }

    func testInit() throws {
        // When:
        let questions = try XCTUnwrap(Questions([question1, question2, question3, nil]))
        
        // Then the expected questions are added:
        XCTAssertTrue(questions.contains(question1!))
        XCTAssertTrue(questions.contains(question3!))
        
        // Then order is maintained:
        let questionAtIndex0 = try XCTUnwrap(questions.getQuestion(at: 0))
        XCTAssertEqual(questionAtIndex0.question, question1.question)
        let questionAtIndex1 = try XCTUnwrap(questions.getQuestion(at: 1))
        XCTAssertEqual(questionAtIndex1.question, question3.question)
        
        // Then no additional questions are added:
        XCTAssertEqual(questions.get().count, 2)
        
    }
    
    func testInitFailEmpty() throws {
        // Given:
        let empty: [Question] = []
        
        // When:
        let questions = Questions(empty)
        
        // Then the expected questions are added:
        XCTAssertNil(questions)
    }
    
    func testGet() throws {
        // Given:
        let questions = Questions([question1, question2, question3, nil])
        
        // When:
        let unwrapped = try XCTUnwrap(questions)
        let innerQuestions = unwrapped.get()
        
        // Then:
        XCTAssertEqual(innerQuestions.count, 2)
        XCTAssertEqual(innerQuestions, [question1, question3])
    }
    
    func testContains() throws {
        // Given:
        let questions = try XCTUnwrap(Questions([question1, question3]))
        
        // When:
        let containsQuestion = questions.contains(question1)
        let containsQuestion2 = questions.contains(question2)
        let containsBadQuestion = questions.contains(Question("bad", answers: answers)!)
        
        // Then:
        XCTAssertTrue(containsQuestion)
        XCTAssertTrue(containsQuestion2, "Should contain question1, which is equivalent to question2")
        XCTAssertFalse(containsBadQuestion)
    }
    
    func testGetAnswer() throws {
        // Given:
        let questions = try XCTUnwrap(Questions([question1, question3]))
        
        // When:
        let actualQuestion = questions.getQuestion(at: 0)
        let actualQuestion2 = questions.getQuestion(at: 1)
        let actualBadQuestion = questions.getQuestion(at: 10)
        
        // Then:
        XCTAssertEqual(actualQuestion, question1)
        XCTAssertEqual(actualQuestion2, question3)
        XCTAssertNil(actualBadQuestion, "Index should not exist")
    }
    
    func testGetIndex() throws {
        // Given:
        let questions = try XCTUnwrap(Questions([question1, question2, question3, nil]))
        
        // When:
        let actualQuestion = questions.getIndex(for: question1)
        let actualQuestion2 = questions.getIndex(for: question2)
        let actualQuestion3 = questions.getIndex(for: question3)
        let actualBadQuestion = questions.getIndex(for: Question("bad", answers: answers)!)
        
        // Then:
        XCTAssertEqual(actualQuestion, 0)
        XCTAssertEqual(actualQuestion2, 0, "question2 should return index of question1 as they are equal")
        XCTAssertEqual(actualQuestion3, 1)
        XCTAssertNil(actualBadQuestion, "Question should not exist")
    }
    
    func testCount() throws {
        // Given:
        let questions = try XCTUnwrap(Questions([question1, question3]))
        
        // When:
        let count = questions.count
        
        // Then:
        XCTAssertEqual(count, 2)
    }
}
