//
//  InspectionFactory.swift
//  InspectModuleTests
//
//  Created by Chloe Houlihan on 21/07/2021.
//

import InspectionModule

struct AreasFactory {
    let areas: [Area]
    
    init() {
        // If wouldn't use force unwraps in prod code, this is just temporary for debugging
        
        let yes = Answer("Yes", score: 0)
        let no = Answer("No", score: 100)
        let yesNo = Answers([yes, no])
        let sick = Question("Is the patient sick?", answers: yesNo!)
        
        let green = Answer("Green", score: 5)
        let spotty = Answer("Red with white spots", score: 30)
        let normal = Answer("Normal", score: 100)
        let colours = Answers([green, spotty, normal])
        let tongue = Question("What colour is the patient's tongue?", answers: colours!)
        
        let sad = Answer("Sad", score: 5)
        let fine = Answer("Fine", score: 60)
        let great = Answer("Great", score: 100)
        let moods = Answers([sad, fine, great])
        let mood = Question("How happy is the patient?", answers: moods!)
        
        let healthQuestions = Questions([sick, tongue, mood])
        let healthCheck = Inspection(name: "Health Check", questions: healthQuestions!)
        
        
        let alive = Answer("Yes", score: 100)
        let dead = Answer("No", score: 0)
        let lifeAnswers = Answers([alive, dead])
        let life = Question("Is everyone alive?", answers: lifeAnswers!)
        
        let wardQuestions = Questions([life])
        let wardCheck = Inspection(name: "Ward Check", questions: wardQuestions!)
        
        
        let open = Answer("Yes", score: 100)
        let closed = Answer("No", score: 0)
        let openClosed = Answers([open, closed])
        let windows = Question("Are the windows open?", answers: openClosed!)
        
        let doctorWho = Answer("Doctor Who", score: 100)
        let eastenders = Answer("Eastenders", score: 0)
        let friends = Answer("Friends", score: 60)
        let shows = Answers([doctorWho, eastenders, friends])
        let tv = Question("Which TV programme is playing?", answers: shows!)
        
        let specialQuestions = Questions([windows, tv])
        let specialCheck = Inspection(name: "Ward B Special Check", questions: specialQuestions!)
        
        
        let inspectionsA: Set = [healthCheck!, wardCheck!]
        let inspectionsB: Set = [healthCheck!, wardCheck!, specialCheck!]
        
        let warda = Area(name: "Ward A", inspections: inspectionsA)
        let wardb = Area(name: "Ward B", inspections: inspectionsB)
        
        areas = [warda!, wardb!]
    }
}
