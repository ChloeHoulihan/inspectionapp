//
//  LoadInspectionViewModel.swift
//  InspectModule
//
//  Created by Chloe Houlihan on 17/07/2021.
//

import InspectionModule
import AppComponents

/// Errors that can be shown by the View
public enum LoadError {
    case failed
}

public class LoadInspectionViewModel: ObservableObject {
    // MARK: - Copy
    public let errorText = "No saves for this user"
    public let loadingText = "Loading..."
    public let redirectingText = "Redirecting..."
    public let title = "Choose inspection to load"
    
    // MARK: - Public fields
    
    @Published public var selectedMemento: InspectionMemento?
    @Published public private(set) var error: LoadError?
    @Published public private(set) var loading: Bool
    @Published public private(set) var mementos: [InspectionMemento]
    
    // MARK:- Private fields
    private let persistence: PersistenceManager
    
    public init(persistence: PersistenceManager) {
        self.error = nil
        self.loading = false
        self.mementos = []
        self.persistence = persistence
        self.selectedMemento = nil
        
        loadInspections()
    }
    
    public func loadInspections() {
        loading = true
        mementos = persistence.loadInspections()
        loading = false
    }
    
    /// Produces a user readable identifier for an inspection
    public func getSaveName(memento: InspectionMemento) -> String {
        return "\(memento.area.name): \(memento.inspection.name)"
    }
}
