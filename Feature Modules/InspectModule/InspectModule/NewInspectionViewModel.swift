//
//  InspectionSelectionViewModel.swift
//  InspectModule
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import InspectionModule
import Combine

public enum NewInspectionError: Error {
    case failedToFetch
}

public class NewInspectionViewModel: ObservableObject {
    // MARK:- Copy
    
    public let areaTitle = "Choose area"
    public let errorText = "Sorry, there are no areas available to you"
    public let inspectionTitle = "Choose inspection"
    public let loadingText = "Loading..."
    public let nothingSelectedErrorText = "Something went wrong. Area and Inspection not nil, but screen not changed."
    
    // MARK:- Public fields
    
    @Published public private(set) var areas: [Area]
    @Published public private(set) var inspections: [Inspection]?
    @Published public private(set) var loading: Bool
    @Published public var selectedArea: Area? {
        didSet {
            loadInspections()
        }
    }
    @Published public var selectedInspection: Inspection?
    
    // MARK:- Private fields
    
    @Published private var areaSubscription: AnyCancellable?
    private let dataProvider: InspectDataProviderProtocol
    
    // MARK:- Methods
    
    public init(dataProvider: InspectDataProviderProtocol) {
        self.loading = false
        self.dataProvider = dataProvider
        self.areas = []
        self.inspections = []
        self.selectedArea = nil
        self.selectedInspection = nil
    }
    
    /// Load areas asynchronously from data provider
    public func loadAreas() {
        self.loading = true
        areaSubscription = dataProvider.areaPublisher()
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] _ in
                guard let self = self else { return }
                self.loading = false
            }, receiveValue: { [weak self] response in
                guard let self = self else { return }
                self.loading = false
                self.areas = response
            })
    }
    
    /// Set inspections based on chosen area
    public func loadInspections() {
        guard let selectedArea = selectedArea else { return }
        inspections = Array(selectedArea.inspections)
    }
}
