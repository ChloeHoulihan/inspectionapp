//
//  InspectionViewModel.swift
//  InspectModule
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import Combine
import InspectionModule
import AppComponents

/// Possible errors for the View to display
public enum InspectionError {
    case noPrevious
    case badAnswer
}

/// A view model acts as a façade of InspectionModel, parsing data for the front-end
public class InspectionViewModel: ObservableObject {
    // MARK: - Copy
    
    public var areaText: String {
        model.area.name
    }
    public let closeText = "Close"
    public let genericErrorText = "Whoops! Something went wrong..."
    public let inspectionText = "Inspection"
    public let reviewText = "Review"
    public var saveBannerText: String {
        if let saveResult = saveResult {
            return saveResult ? "Saved successfully" : "Save failed"
        } else {
            return ""
        }
    }
    public let saveLaterText = "Save for later"
    public var scoreText: String {
        "\(score) out of \(maxScore)"
    }
    public let startText = "Start"
    public let submitAndReviewText = "Submit and Review"
    public let submitText = "Submit"
    public var title: String { model.inspection.name }
    
    // MARK:- Public fields
    
    @Published public var error: InspectionError?
    @Published public var selectedAnswer: Answer?
    
    @Published public private(set) var saveResult: Bool?
    @Published public private(set) var isConnectedToInternet: Bool
    @Published public private(set) var currentQuestion: Question?
    @Published public private(set) var isComplete: Bool
    @Published public private(set) var score: Int
    @Published public private(set) var maxScore: Int
    
    // MARK:- Private fields
    
    private let dataProvider: InspectDataProvider
    private let persistence: PersistenceManager
    private var model: InspectionModel
    private var subscriptions = Set<AnyCancellable>()
    
    // MARK:- Inits
    public init(model: InspectionModel, dataProvider: InspectDataProvider, persistence: PersistenceManager) {
        self.isConnectedToInternet = true // TODO: Dynamic checking
        
        self.model = model
        self.dataProvider = dataProvider
        self.persistence = persistence
        self.error = nil
        self.saveResult = nil
        
        self.selectedAnswer = nil
        self.isComplete = model.isLastQuestion
        self.currentQuestion = model.currentQuestion
        self.score = model.score
        self.maxScore = model.maxScore
        
        // Dismiss save banner when user changes answer
        self.$selectedAnswer.sink { [weak self] _ in
            self?.saveResult = nil
        }.store(in: &self.subscriptions)
    }
    
    public convenience init?(area: Area, inspection: Inspection, dataProvider: InspectDataProvider, persistence: PersistenceManager) {
        guard let model = InspectionModel(area: area, inspection: inspection) else { return nil }
        self.init(model: model, dataProvider: dataProvider, persistence: persistence)
    }
    
    public convenience init?(memento: InspectionMemento, dataProvider: InspectDataProvider, persistence: PersistenceManager) {
        guard let model = InspectionModel(from: memento) else { return nil }
        self.init(model: model, dataProvider: dataProvider, persistence: persistence)
    }
    
    // MARK:- View callable methods
    
    /// Convert the results to user readable format for the View
    public func getResult() -> [(question: String, answer: String)] {
        var result: [(question: String, answer: String)] = []
        for question in model.inspection.questions.get() {
            let questionText = question.question
            let answerText = model.getAnswer(for: question)?.answer ?? "Not answered"
            result.append((question: questionText, answer: answerText))
        }
        return result
    }
    
    public func nextQuestion() {
        guard let selectedAnswer = selectedAnswer else { return }
        guard let currentQuestion = currentQuestion else { return }
        
        let success = model.answer(question: currentQuestion, with: selectedAnswer)
        guard success else {
            error = .badAnswer
            return
        }
        
        self.selectedAnswer = nil
        saveResult = nil
        let _ = model.nextQuestion() // Failure doesn't matter, we go to review view
        updateValues()
    }
    
    public func previousQuestion() {
        // TODO: Implement functionality for user pressing < on navbar, allowing user to go back and change answers
    }
    
    public func submit() {
        dataProvider.submitInspection(memento: model.createMemento())
    }
    
    public func save() {
        saveResult = persistence.save(inspection: model.createMemento())
    }
    
    // MARK:- Private methods
    
    // You may be wondering why I didn't just put everything in the Model in the VM.
    // Because this way the Model can be reused by other VMs that do not need to display
    // some of these values. This way the VM only exposes values needed by the View, instead
    // of everything by default.
    private func updateValues() {
        self.currentQuestion = model.currentQuestion
        self.isComplete = model.isLastQuestion // TODO: Change to isComplete when previousQuestion implemented
        self.score = model.score
        self.maxScore = model.maxScore
    }
}
