//
//  InspectDataProvider.swift
//  InspectModule
//
//  Created by Chloe Houlihan on 21/07/2021.
//

import AppComponents
import Combine
import InspectionModule
import ServicesSDK

/// Data provider protocol to send and receive inspection data
/// Acts as a façade of whatever backend/mock is used
public protocol InspectDataProviderProtocol {
    func areaPublisher() -> AreaPublisher
    
    func submitInspection(memento: InspectionMemento)
}

public typealias AreaPublisher = AnyPublisher<[InspectionModule.Area], NewInspectionError>

public typealias AreaFuture = Future<[InspectionModule.Area], NewInspectionError>

/// Manages SDK calls and conversion to our data models. This way the SDK need only be imported here.
public class InspectDataProvider: InspectDataProviderProtocol {
    private let context: Context
    
    public init(context: Context) {
        self.context = context
    }
    
    // MARK:- Publishers
    
    /// Requests user's areas from backend, converts the results, and returns completion and results asynchronously
    public func areaPublisher() -> AreaPublisher {
        AreaFuture { [weak self] promise in
            guard let self = self else {
                promise(.failure(.failedToFetch))
                return
            }
            guard let userID = self.context.userID else {
                promise(.failure(.failedToFetch))
                return
            }
            self.context.services.fetchInspectionData(for: userID) { result in
                switch result {
                case .failure:
                    // TODO: Convert SDK errors to our own errors
                    promise(.failure(.failedToFetch))
                case .success(let data):
                    promise(.success(self.convertToAreas(data)))
                }
            }
        }.eraseToAnyPublisher()
    }
    
    /// Creates an SDK model from the memento and sends this to backend
    public func submitInspection(memento: InspectionMemento) {
        let submission = createSubmission(memento: memento)
        
        context.services.sendInspectionResult(submission: submission) { _ in
            // TODO: Forward result to services
        }
    }
    
    // MARK: - Private creators and converters
    
    private func createSubmission(memento: InspectionMemento) -> InspectionSubmission {
        return InspectionSubmission(area: memento.area.name,
                                    inspection: memento.inspection.name,
                                    score: memento.result.currentScore,
                                    maxScore: memento.result.maxScore)
    }
    
    private func convertToAreas(_ data: InspectionData) -> [InspectionModule.Area] {
        var areas: [InspectionModule.Area] = []
        for area in data.areas {
            if let newArea = convertAreaToArea(area: area) {
                areas.append(newArea)
            }
        }
        return areas
    }
    
    private func convertAreaToArea(area: ServicesSDK.Area) -> InspectionModule.Area? {
        let inspections = area.inspections
        var newInspections: [InspectionModule.Inspection] = []
        for inspection in inspections {
            if let newInspection = convertInspectionToInspection(inspection: inspection) {
                newInspections.append(newInspection)
            }
        }
        return Area(name: area.name, inspections: Set(newInspections))
    }
    
    private func convertInspectionToInspection(inspection: ServicesSDK.Inspection) -> InspectionModule.Inspection? {
        let questions = inspection.questions
        var newQuestionBuffer: [InspectionModule.Question] = []
        for question in questions {
            if let newQuestion = convertQuestionToQuestion(question: question) {
                newQuestionBuffer.append(newQuestion)
            }
        }
        guard let newQuestions = Questions(newQuestionBuffer) else {
            return nil
        }
        return Inspection(name: inspection.name, questions: newQuestions)
    }
    
    private func convertQuestionToQuestion(question: ServicesSDK.Question) -> InspectionModule.Question? {
        let answers = question.answers
        var newAnswerBuffer: [InspectionModule.Answer] = []
        for answer in answers {
            if let newAnswer = convertAnswerToAnswer(answer: answer) {
                newAnswerBuffer.append(newAnswer)
            }
        }
        guard let newAnswers = Answers(newAnswerBuffer) else {
            return nil
        }
        return Question(question.content, answers: newAnswers)
    }
    
    private func convertAnswerToAnswer(answer: ServicesSDK.Answer) -> InspectionModule.Answer? {
        return Answer(answer.content, score: answer.score)
    }
}
