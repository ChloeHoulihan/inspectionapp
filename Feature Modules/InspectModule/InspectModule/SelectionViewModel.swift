//
//  SelectionViewModel.swift
//  InspectModule
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import Combine

public class SelectionViewModel: ObservableObject {
    // MARK:- Copy
    
    public let title = "Select Inspection"
    public let newText = "New"
    public let loadText = "Load"
    
    // MARK: - Public fields
    
    @Published public var showingNewSheet: Bool = false
    @Published public var showingLoadSheet: Bool = false
    
    // MARK: - Methods
    
    public init() { }
    
}
