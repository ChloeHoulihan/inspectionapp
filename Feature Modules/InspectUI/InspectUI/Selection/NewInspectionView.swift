//
//  AreaSelectionView.swift
//  InspectUI
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import InspectModule
import SwiftUI
import UIComponents
import InspectionModule

public protocol NewInspectionViewRouter: AnyObject {
    func instantiateInspectionView(area: Area, inspection: Inspection)
}

public struct NewInspectionView: View {
    @State var router: NewInspectionViewRouter
    @ObservedObject private var viewModel: NewInspectionViewModel
    
    public init(router: NewInspectionViewRouter, viewModel: NewInspectionViewModel) {
        self.router = router
        self.viewModel = viewModel
    }
    
    public var body: some View {
        rootView
            .onAppear {
                viewModel.loadAreas()
            }
    }
    
    @ViewBuilder public var rootView: some View {
        if viewModel.loading {
            Text(viewModel.loadingText)
        } else if viewModel.areas.isEmpty {
            Text(viewModel.errorText)
        } else if viewModel.selectedArea == nil {
            areaSelectionView
        } else if viewModel.selectedInspection == nil {
            inspectionSelectionView
        } else {
            Text(viewModel.nothingSelectedErrorText)
        }
    }
    
    private var areaSelectionView: some View {
        VStack {
            Text(viewModel.areaTitle)
                .titleStyle()
                .padding(.vertical, Sizes.verticalPadding)
            ForEach(viewModel.areas, id: \.self) { area in
                NavLinkRow(text: area.name)
                    .padding(.vertical, Sizes.verticalPadding)
                    .onTapGesture {
                        viewModel.selectedArea = area
                    }
            }
            Spacer()
        }
    }
    
    private var inspectionSelectionView: some View {
        VStack {
            Text(viewModel.inspectionTitle).titleStyle()
                .padding(.vertical, Sizes.verticalPadding)
            Divider()
            ScrollView {
                if let inspections = viewModel.inspections {
                    ForEach(inspections, id: \.self) { inspection in
                        NavLinkRow(text: inspection.name)
                            .padding(.vertical, Sizes.verticalPadding)
                            .onTapGesture {
                                viewModel.selectedInspection = inspection
                                if let area = viewModel.selectedArea {
                                    router.instantiateInspectionView(area: area, inspection: inspection)
                                }
                            }
                    }
                }
            }
            Spacer()
        }
        .padding(.vertical, Sizes.verticalPadding)
    }
}
