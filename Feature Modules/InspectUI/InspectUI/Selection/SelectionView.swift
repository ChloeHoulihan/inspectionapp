//
//  InspectView.swift
//  InspectUI
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import InspectModule
import SwiftUI
import UIComponents

public protocol SelectionViewRouter: AnyObject {
    func instantiateNewSelectionView() -> NewInspectionView
    
    func instantiateLoadInspectionView() -> LoadInspectionView
}

public struct SelectionView: View {
    @State var router: SelectionViewRouter
    @ObservedObject private var viewModel: SelectionViewModel
    
    public init(router: SelectionViewRouter, viewModel: SelectionViewModel) {
        self.router = router
        self.viewModel = viewModel
    }
    
    public var body: some View {
        VStack {
            Text(viewModel.title).titleStyle()
            newInspectionRow
            loadInspectionRow
            Spacer()
        }
    }
    
    private var newInspectionRow: some View {
        HStack(spacing: .zero) {
            NavLinkRow(text: viewModel.newText)
                .onTapGesture {
                    viewModel.showingNewSheet = true
                }
        }
        .sheet(isPresented: $viewModel.showingNewSheet) {
            router.instantiateNewSelectionView()
        }
    }
    
    private var loadInspectionRow: some View {
        HStack(spacing: .zero) {
            NavLinkRow(text: viewModel.loadText)
                .onTapGesture {
                    viewModel.showingLoadSheet = true
                }
        }
        .sheet(isPresented: $viewModel.showingLoadSheet) {
            router.instantiateLoadInspectionView()
        }
    }
}
