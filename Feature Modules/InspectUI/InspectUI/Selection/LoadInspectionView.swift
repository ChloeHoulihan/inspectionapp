//
//  LoadInspectionView.swift
//  InspectUI
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import InspectionModule
import InspectModule
import SwiftUI
import UIComponents

public protocol LoadInspectionViewRouter: AnyObject {
    func instantiateInspectionView(memento: InspectionMemento)
}

public struct LoadInspectionView: View {
    @State var router: LoadInspectionViewRouter
    @ObservedObject private var viewModel: LoadInspectionViewModel
    
    public init(router: LoadInspectionViewRouter, viewModel: LoadInspectionViewModel) {
        self.router = router
        self.viewModel = viewModel
    }
    
    public var body: some View {
        if viewModel.loading {
            loadingView
        } else if let validMemento = viewModel.selectedMemento {
            redirectingView(memento: validMemento)
        } else if viewModel.mementos.isEmpty {
            errorView
        } else {
            selectFileView
        }
    }
    
    private var errorView: some View {
        Text(viewModel.errorText)
    }
    
    private var selectFileView: some View {
        VStack {
            Text(viewModel.title)
                .titleStyle()
                .padding(.vertical, Sizes.verticalPadding)
            Divider()
            ScrollView {
                ForEach(viewModel.mementos, id: \.self) { memento in
                    NavLinkRow(text: viewModel.getSaveName(memento: memento)).onTapGesture {
                        viewModel.selectedMemento = memento
                    }
                }
            }
            Spacer()
        }
    }
    
    private var loadingView: some View {
        Text(viewModel.loadingText)
    }
     
    @ViewBuilder
    private func redirectingView(memento: InspectionMemento) -> some View {
        Text(viewModel.redirectingText)
            .onAppear {
                router.instantiateInspectionView(memento: memento)
            }
    }
}
