//
//  InspectLabel.swift
//  InspectUI
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import SwiftUI

public struct InspectLabel: View {
    
    public init() {}
    
    public var body: some View {
        Label("Inspect", systemImage: "doc.on.clipboard")
    }
}
