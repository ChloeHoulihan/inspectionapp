//
//  QuestionView.swift
//  InspectUI
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import InspectionModule
import InspectModule
import SwiftUI
import UIComponents

public protocol QuestionViewRouter: AnyObject {
    func instantiateQuestionView(viewModel: InspectionViewModel) -> QuestionView
    
    func instantiateReviewView(viewModel: InspectionViewModel) -> ReviewView
    
    func instantiateSelectionView()
}

public struct QuestionView: View {
    @State private var router: QuestionViewRouter
    @ObservedObject private var viewModel: InspectionViewModel
    
    public init(router: QuestionViewRouter, viewModel: InspectionViewModel) {
        self.router = router
        self.viewModel = viewModel
    }
    
    public var body: some View {
        VStack {
            // TODO: Use viewModel.error to display error banners
            SaveBanner(viewModel: viewModel)
            rootView
            Spacer()
        }
        .navigationTitle(viewModel.title)
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarItems(trailing:
            HStack {
                SystemImages.Save()
                    .onTapGesture {
                        viewModel.save()
                    }
                Spacer()
                SystemImages.Trash()
                    .onTapGesture {
                        router.instantiateSelectionView()
                    }
            }
        )
    }
    
    private var rootView: some View {
        VStack {
            if let currentQuestion = viewModel.currentQuestion {
                questionView(question: currentQuestion)
            } else {
                errorView
            }
        }
    }
    
    private func questionView(question: Question) -> some View {
        VStack {
            Text(question.question)
                .titleStyle()
                .padding(.vertical, Sizes.verticalPadding)
            AnswerTickList(list: question.answers.get(),
                     selected: $viewModel.selectedAnswer)
            button.disabled(viewModel.selectedAnswer == nil)
        }
    }
    
    @ViewBuilder
    private var button: some View {
        if viewModel.isComplete {
            NavigationLink(destination: destination) {
                Text(viewModel.submitAndReviewText)
            }
            .buttonStyle(AcceptenceButtonStyle())
            .simultaneousGesture(TapGesture().onEnded{
                viewModel.nextQuestion()
            })
        } else {
            Button(viewModel.submitText) {
                viewModel.nextQuestion()
            }.buttonStyle(AcceptenceButtonStyle())
        }
    }
    
    private var errorView: some View {
        Text(viewModel.genericErrorText)
    }
    
    private var destination: some View {
        router.instantiateReviewView(viewModel: viewModel)
    }
}

public struct AnswerTickList: View {
    @Binding private var selected: Answer?
    private let list: [Answer]
    
    public init(list: [Answer], selected: Binding<Answer?>) {
        self.list = list
        _selected = selected
    }
    
    public var body: some View {
        ForEach(list, id: \.answer) { item in
            AnswerTickRow(item: item, selected: $selected)
                .onTapGesture {
                    selected = item
                }
        }
    }
}

/**
 * Check out UIComponents/Views for how I'd like to have made these reusable:
 */

public struct AnswerTickRow: View {
    /// The answer selected in the list
    @Binding private var selected: Answer?
    /// This answer
    private let item: Answer
    /// Whether this answer is selected in the list
    private var isSelected: Bool {
        selected?.answer == item.answer
    }
    
    public init(item: Answer, selected: Binding<Answer?>) {
        self.item = item
        _selected = selected
    }
    
    public var body: some View {
        HStack(spacing: .zero) {
            Text(item.answer).subtitleStyle()
            Spacer()
            if isSelected {
                SystemImages.Checkmark().foregroundColor(.black)
            }
        }
        .padding(.horizontal, Sizes.horizontalPadding)
        .padding(.vertical, Sizes.verticalPadding)
        .background(isSelected ? Color.gray : Color.white)
    }
}
