//
//  SaveBanner.swift
//  InspectUI
//
//  Created by Chloe Houlihan on 21/07/2021.
//

import SwiftUI
import InspectModule
import UIComponents

public struct SaveBanner: View {
    @ObservedObject private var viewModel: InspectionViewModel
    
    public init(viewModel: InspectionViewModel) {
        self.viewModel = viewModel
    }
    
    public var body: some View {
        if viewModel.saveResult != nil {
            VStack {
                Text(viewModel.saveBannerText).paragraphStyle()
                    .padding(.vertical, Sizes.verticalPadding)
                Spacer()
            }
            .frame(maxWidth: .infinity)
            .background(Color.yellow)
            .padding(.vertical, Sizes.verticalPadding)
            .fixedSize(horizontal: false, vertical: true)
        } else {
            EmptyView()
        }
    }
}
