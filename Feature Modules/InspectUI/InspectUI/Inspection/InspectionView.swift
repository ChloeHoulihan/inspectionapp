//
//  InspectionView.swift
//  InspectUI
//
//  Created by Chloe Houlihan on 17/07/2021.
//

import InspectModule
import SwiftUI
import UIComponents

public protocol InspectionViewRouter: AnyObject {
    func instantiateQuestionView(viewModel: InspectionViewModel) -> QuestionView
    
    func instantiateSelectionView()
}

public struct InspectionView: View {
    @State var router: InspectionViewRouter
    @ObservedObject private var viewModel: InspectionViewModel
    
    public init(router: InspectionViewRouter, viewModel: InspectionViewModel) {
        self.router = router
        self.viewModel = viewModel
    }
    
    public var body: some View {
        NavigationView {
            rootView
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
    
    private var rootView: some View {
        VStack {
            Text(viewModel.areaText).subtitleStyle()
                .padding(.horizontal, Sizes.horizontalPadding)
                .padding(.vertical, Sizes.verticalPadding)
            Divider()
            Text(viewModel.title).titleStyle()
                .padding(.horizontal, Sizes.horizontalPadding)
                .padding(.vertical, Sizes.verticalPadding)
            SystemImages.Inspect()
                .foregroundColor(.blue)
                .padding(.horizontal, Sizes.horizontalPadding)
                .padding(.vertical, Sizes.verticalPadding)
            
            Spacer().frame(height: 20)
            
            NavigationLink(destination: router.instantiateQuestionView(viewModel: viewModel)) {
                Text(viewModel.startText)
            }
            .buttonStyle(AcceptenceButtonStyle())
            Spacer()
        }
        .navigationTitle(viewModel.inspectionText)
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarItems(trailing:
            SystemImages.Trash()
                .onTapGesture {
                    router.instantiateSelectionView()
                }
        )
    }
}
