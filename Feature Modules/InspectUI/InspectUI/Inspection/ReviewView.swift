//
//  ReviewView.swift
//  InspectUI
//
//  Created by Chloe Houlihan on 16/07/2021.
//

import InspectModule
import SwiftUI
import UIComponents

public protocol ReviewViewRouter: AnyObject {
    func instantiateSelectionView()
}

public struct ReviewView: View {
    @State var router: ReviewViewRouter
    @ObservedObject private var viewModel: InspectionViewModel
    
    public init(router: ReviewViewRouter, viewModel: InspectionViewModel) {
        self.router = router
        self.viewModel = viewModel
    }
    
    public var body: some View {
        VStack {
            SaveBanner(viewModel: viewModel)
            rootView
            Spacer()
        }
        .navigationTitle(viewModel.reviewText)
        .navigationBarTitleDisplayMode(.inline)
    }
    
    private var rootView: some View {
        VStack {
            Text(viewModel.title).titleStyle()
                .padding(.vertical, Sizes.verticalPadding)
                .padding(.horizontal, Sizes.horizontalPadding)
            Text(viewModel.scoreText).subtitleStyle()
                .padding(.vertical, Sizes.verticalPadding)
                .padding(.horizontal, Sizes.horizontalPadding)
            
            ScrollView {
                VStack {
                    ForEach(viewModel.getResult(), id: \.question) { result in
                        Text(result.question).subtitleStyle()
                            .padding(.vertical, Sizes.verticalPadding)
                            .padding(.horizontal, Sizes.horizontalPadding)
                        Text(result.answer).paragraphStyle()
                            .padding(.vertical, Sizes.verticalPadding)
                            .padding(.horizontal, Sizes.horizontalPadding)
                        Divider()
                    }
                }
            }
            .border(Color.gray)
            
            VStack {
                Button(viewModel.submitText) {
                    viewModel.submit()
                    router.instantiateSelectionView()
                }
                .disabled(!viewModel.isConnectedToInternet)
                .buttonStyle(AcceptenceButtonStyle())
                
                Button(viewModel.saveLaterText) {
                    viewModel.save()
                    // TODO: Play animation to confirm saving before changing view
                    if viewModel.saveResult == true {
                        router.instantiateSelectionView()
                    }
                    // If save failed, save banner will indicate as such
                }
                .buttonStyle(AcceptenceButtonStyle())
            }
            .padding(.vertical, Sizes.verticalPadding)
            .padding(.horizontal, Sizes.horizontalPadding)
        }
    }
}

