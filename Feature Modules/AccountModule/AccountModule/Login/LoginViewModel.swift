//
//  LoginViewModel.swift
//  AccountModule
//
//  Created by Chloe Houlihan on 22/07/2021.
//

import AppComponents

public class LoginViewModel: ObservableObject {
    
    // MARK:- Copy
    public let buttonText = "Login"
    public let loginFailedText = "Login failed"
    public let passwordText = "Password"
    public let title = "Inspection App"
    public let usernameText = "Username"
    
    // MARK:- Public fields
    @Published public var error: String?
    
    // MARK: - Private fields
    private var context: Context
    
    public init(context: Context) {
        self.context = context
    }
    
    public func login(userId: String, password: String) {
        // TODO: Send details to context.loginManager to check
        
        // Placeholder code:
        context.userID = userId
    }
}
