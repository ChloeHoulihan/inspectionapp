//
//  AccountViewModel.swift
//  AccountModule
//
//  Created by Chloe Houlihan on 22/07/2021.
//

import AppComponents

public class AccountViewModel: ObservableObject {
    
    // MARK:- Copy
    
    public let logoutText = "Log out"
    public let title = "Account"
    public var username: String {
        context.userID ?? "User not found"
    }
    
    // MARK:- Private fields
    
    private var context: Context
    
    // MARK:- Init
    public init(context: Context) {
        self.context = context
    }
    
    public func logout() {
        context.userID = nil
    }
}
