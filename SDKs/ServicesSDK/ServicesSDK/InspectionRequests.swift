//
//  Inspection.swift
//  InspectionSDK
//
//  Created by Chloe Houlihan on 20/07/2021.
//

public struct Answer: Decodable {
    public let content: String
    public let score: Int
}

public struct Question: Decodable {
    public let content: String
    public let answers: [Answer]
}

public struct Inspection: Decodable {
    public let name: String
    public let questions: [Question]
}

public struct Area: Decodable {
    public let name: String
    // If doing this again, I would consider just list inspection IDs (names) here
    // and fetch both areas and inspections to reduce repetition in the JSON.
    // Obviously JSON format wouldn't be decided by me anyway.
    public let inspections: [Inspection]
}

public struct InspectionData: Decodable {
    public let areas: [Area]
}
