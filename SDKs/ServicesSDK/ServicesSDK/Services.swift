//
//  AppServices.swift
//  ServicesSDK
//
//  Created by Chloe Houlihan on 20/07/2021.
//

import Alamofire

/// Conform in order to fetch the required data from any backend of choice
public protocol Services {
    func fetchInspectionData(for userID: String, result: @escaping (Result<InspectionData, ServicesError>) -> Void)
    
    func sendInspectionResult(submission: InspectionSubmission, result: @escaping (Result<
                                Bool, ServicesError>) -> Void)
}

/// This class would connect to the actual backend API (which I don't have)
public class AppServices: Services {
    public init() { }
    
    public func fetchInspectionData(for userID: String, result: @escaping (Result<InspectionData, ServicesError>) -> Void) {
        // TODO: Replace with actual API call
    }
    
    public func sendInspectionResult(submission: InspectionSubmission, result: @escaping (Result<Bool, ServicesError>) -> Void) {
        // TODO: Replace with actual API call
    }
}

/// This class will be used by InspectionApp for now.
/// Instead of connecting to a backend, it grabs data from JSON files.
/// I could also have fleshed out AppServices and injected a mock URLSession, but I didn't have time.
public class MockServices: Services {
    public init() { }
    
    public func fetchInspectionData(for userID: String, result: @escaping (Result<InspectionData, ServicesError>) -> Void) {
        let dispatchQueue = DispatchQueue(label: "inspection-data")
        let dGroup = DispatchGroup()
        
        dispatchQueue.async {
            dGroup.enter()
            
            // Obviously I wouldn't do anything like this in production code:
            do {
                let url = Bundle(for: MockServices.self).path(forResource: "mock", ofType: "json")!
                let jsonData = try String(contentsOfFile: url).data(using: .utf8)!
                let response: InspectionData = try! JSONDecoder().decode(InspectionData.self, from: jsonData)
                
                dGroup.leave()
                
                dGroup.notify(queue: dispatchQueue) {
                    result(.success(response))
                }
            } catch {
                
            }
        }
    }
    
    public func sendInspectionResult(submission: InspectionSubmission, result: @escaping (Result<Bool, ServicesError>) -> Void) {
        let dispatchQueue = DispatchQueue(label: "inspection-submission")
        let dGroup = DispatchGroup()
        
        dispatchQueue.async {
            
            dGroup.enter()
            
            dGroup.notify(queue: dispatchQueue) {
                result(.success(true))
            }
        }
    }
}

public enum ServicesError: Error {
    // TODO: Actual errors
    // case jsonNotOfType
    // case NetworkFailure
    case somethingWentWrong
}
