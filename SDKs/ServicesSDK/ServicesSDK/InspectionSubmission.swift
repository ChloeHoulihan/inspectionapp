//
//  InspectionSubmission.swift
//  InspectionSDK
//
//  Created by Chloe Houlihan on 20/07/2021.
//

public struct InspectionSubmission: Decodable {
    let area: String
    let inspection: String
    let score: Int
    let maxScore: Int
    
    public init(area: String, inspection: String, score: Int, maxScore: Int) {
        self.area = area
        self.inspection = inspection
        self.score = score
        self.maxScore = maxScore
    }
}
