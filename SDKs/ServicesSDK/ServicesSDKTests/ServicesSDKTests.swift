//
//  ServicesSDKTests.swift
//  ServicesSDKTests
//
//  Created by Chloe Houlihan on 20/07/2021.
//

import XCTest
import ServicesSDK

class ServicesSDKTests: XCTestCase {
    
    func testJsonFile() throws {
        let url = Bundle(for: MockServices.self).path(forResource: "mock", ofType: "json")!
        let jsonData = try String(contentsOfFile: url).data(using: .utf8)!
        let response: InspectionData = try! JSONDecoder().decode(InspectionData.self, from: jsonData)
        
        XCTAssertFalse(response.areas.isEmpty)
    }

    func testMockServicesFetchInspectionData() {
        let services = MockServices()
        
        let expectation = XCTestExpectation(description: "Fetch inspection data should finish")
        var actualResponse: Result<InspectionData, ServicesError>!

        services.fetchInspectionData(for: "User") { response in
            actualResponse = response
            expectation.fulfill()
        }

        XCTWaiter().wait(for: [expectation], timeout: 5)

        switch actualResponse {
        case .failure:
            XCTFail()
        case .success(let inspectionData):
            XCTAssertFalse(inspectionData.areas.isEmpty)
        case .none:
            XCTFail()
        }
    }
}
